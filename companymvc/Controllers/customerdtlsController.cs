﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using companymvc.Models;
using businesslayer;
using System.Data;
using commonmodel;
namespace companymvc.Controllers
{
    public class customerdtlsController : Controller
    {
        dbcustomers obd = new dbcustomers();
        // GET: costomerdts
        public ActionResult Index()
        {
            dbcustomers da = new dbcustomers();
            DataSet di = da.getcustomers();
            IList<custom> chls = new List<custom>();
            //foreach (DataRow dr in di.Tables[0].Rows)
            // {
            //    custom df = new custom();
            //    df.Id = Convert.ToInt32(dr[0]);
            //    df.Name = Convert.ToString(dr[1]);
            //    df.Address = Convert.ToString(dr[2]);
            //    df.MobileNumber = Convert.ToString(dr[3]);
            //    df.Mail = Convert.ToString(dr[4]);
            //    chls.Add(df);
            //}
            chls = (from DataRow dr in di.Tables[0].Rows
                    select new custom()
                    {
                        Id = Convert.ToInt32(dr[0]),
                        Name = Convert.ToString(dr[1]),
                        Address = Convert.ToString(dr[2]),
                        MobileNumber = Convert.ToString(dr[3]),
                        Mail = Convert.ToString(dr[4])
                    }).ToList();
            TempData["updatacustomer"] = chls;
            return View(chls);
        }
        

        // GET: customerdtls/Details/5
        public ActionResult Details(int id)
        {
            IList<custom> chlst = TempData["updatacustomer"] as List<custom>;
            var custo = chlst.Where(x => x.Id == id).FirstOrDefault();
            return View(custo);
        }

        // GET: customerdtls/Create
        public ActionResult Create()
        {
            
            return View();
        }

        // POST: customerdtls/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {

                ddclass obj = new ddclass();
                obj.Name = collection["Name"];
                obj.Address = collection["Address"];
                obj.MobileNumber = collection["MobileNumber"];
                obj.Mail = collection["Mail"];

                obd.insertcustomer(obj);
                return RedirectToAction("Index");
                
            }
            catch
            {
                return View();
            }
        }

        // GET: customerdtls/Edit/5
        public ActionResult Edit( int id)
        {
           
            IList<custom> chlst = TempData["updatacustomer"] as List<custom>;
            var custom =( chlst.Where(x => x.Id ==id).FirstOrDefault());

            

            
            return View(custom);

        }
        public ActionResult submit()
        {
            //var Id = somu.Id;
            //var Name = somu.Name;
            //var Address = somu.Address;
            //var MobileNumber = somu.MobileNumber;
            //var Mail = somu.Mail;
            //ar student = studentList.Where(s => s.StudentId == std.StudentId).FirstOrDefault();
            //studentList.Remove(student);
            //studentList.Add(std);
            


            return RedirectToAction("Index");
        }

        // POST: customerdtls/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, custom cust)
        {
            try
            {
                //IList<custom> chls = new List<custom>();


                //var Id = cust.Id;
                //var Name = cust.Name;
                //var Address = cust.Address;
                //var MobileNumber = cust.MobileNumber;
                //var Mail = cust.Mail;
                ddclass obj = new ddclass();
                obj.Id = cust.Id;
                obj.Name = cust.Name;
                obj.Address = cust.Address;
                obj.MobileNumber = cust.MobileNumber;
                obj.Mail = cust.Mail;

                obd.updataustomer(obj);
                //IList<custom> chlst = TempData["updatacustomer"] as List<custom>;
                //var custom = chlst.Where(x => x.Id == cust.Id).FirstOrDefault();
                //chlst.Remove(custom);
                //chls.Add(cust);





                //TODO: Add update logic here
                
                return RedirectToAction("Index");
                
            }
            catch
            {
                return View();
            }
        }

        // GET: customerdtls/Delete/5
        public ActionResult Delete(int id)
        {
            IList<custom> chlst = TempData["updatacustomer"] as List<custom>;
           var custom = (chlst.Where(x => x.Id == id).FirstOrDefault());

            return View(custom);
        }

        // POST: customerdtls/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, custom collection)
        {
            try
            {
                ddclass obg = new ddclass();

                obg.Id = Convert.ToInt32(collection.Id);
                //collection.Remove();
                obd.deletecustomer(obg);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
