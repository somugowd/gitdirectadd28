﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace companymvc.Models
{
    public class custom
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Name Required field")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Address Required field")]
        public string Address { get; set; }
        //[Display(Name ="MobileNumber")]
        //[Required(ErrorMessage = "MobileNumber is Required")]
        //[RegularExpression(@"^([0-9]{10)$",ErrorMessage ="invalid MobileNumber.")]
        //[DataType(DataType.PhoneNumber)]
        //[DataType(DataType.PhoneNumber)]
        //[Display(Name = "Mobile Number")]
        //[Required(ErrorMessage = "Phone Number Required!")]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$",
        //           ErrorMessage = "Entered phone format is not valid.")]
        [Required(ErrorMessage = "Required ")]
        [RegularExpression(@"^(\d{10})$", ErrorMessage = "Wrong mobile")]
        public string MobileNumber { get; set; }
        [Required(ErrorMessage = "Mail Required field")]
        public string Mail { get; set; }
    }
}