﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using commonmodel;


namespace dataaccesslayer
{
    public class datacustomer
    {
        SqlConnection conn = new SqlConnection("Data Source=DESKTOP-FATGQMV;Initial Catalog=company;Integrated Security=True");

        public DataSet getcustomer()
        {
            SqlCommand kh = new SqlCommand();
            kh.CommandText = "select * from customerinfo";
            kh.CommandType = CommandType.Text;
            kh.Connection = conn;
            SqlDataAdapter hal = new SqlDataAdapter(kh);
            DataSet lm = new DataSet();
            conn.Open();
            hal.Fill(lm, "test");
            conn.Close();
            return lm;
        }

        public void insertcustomer(ddclass somu)
        {
            try
            {
                SqlConnection conn = new SqlConnection("Data Source=DESKTOP-FATGQMV;Initial Catalog=company;Integrated Security=True");

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = "getcompany";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Name", somu.Name);
                cmd.Parameters.AddWithValue("@Address", somu.Address);
                cmd.Parameters.AddWithValue("@MobileNumber", somu.MobileNumber);
                cmd.Parameters.AddWithValue("@Mail", somu.Mail);
                conn.Open();
                cmd.ExecuteNonQuery();    
            }
            catch(Exception ex)
            {
                

            }
            finally
            {
                conn.Close();
            }
            
            
        }
        public void updatacustomer(ddclass somu)
        {
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-FATGQMV;Initial Catalog=company;Integrated Security=True");

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = "updatecustomer";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", somu.Id);
            cmd.Parameters.AddWithValue("@Name", somu.Name);
            cmd.Parameters.AddWithValue("@Address", somu.Address);
            cmd.Parameters.AddWithValue("@MobileNumber", somu.MobileNumber);
            cmd.Parameters.AddWithValue("@Mail", somu.Mail);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

        }
        public void deletecustomer(ddclass gowd)
        {
            try
            {
                SqlConnection conn = new SqlConnection("Data Source=DESKTOP-FATGQMV;Initial Catalog=company;Integrated Security=True");

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = "deletecustomer";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", gowd.Id);
                conn.Open();
                cmd.ExecuteNonQuery();

            }
            catch(Exception ex)
            {

            }
            finally
            {
                conn.Close();
            }
           
            conn.Close();

        }
    }
}
